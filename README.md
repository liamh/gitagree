## Synopsis

Check and bring into agreement a git working directory with any number of remotes if possible by fast-forward merge. No single remote is required to be available. Use with [myrepos](http://myrepos.branchable.com/) is recommended for multiple projects. It is necessary to set the envionment variable USEREMOTES to a list of all the desired remotes; anything not on this list will be skipped.

## Example

## Motivation
I have multiple computers and my projects have multiple remotes not all of which are available all the time to all the computers. The vast majority of my merges are fast-forward, which means they can be done automatically. I run this script (through myrepos) at the beginning and end of every session, and it makes sure all the available repositories are brought into sync with the local directories, and vice versa. 

The intent is that this script runs fast: if a remote is not available, it skips over it. Every attempt has been made to disable blocking for network access, passwords/passphrases, etc.; if authorization is not present it should just mark the remote as unavailable and move on. This is not perfect and it is possible that there will be blocking depending on the type of remote. As a consequence, the user is expected to make sure network access is present, file systems are mounted, and authentication completed before starting this script.

## Requirements

Bash, Linux. All the Linux-specific commands are devoted to making the script non-blocking and so could be adapted to a different OS with some knowledge and work.

## Tests

Describe and show how to run the tests with code examples.

## License

BSD [2-clause](http://opensource.org/licenses/BSD-2-Clause)
