#!/bin/bash
# Shows/brings all branches into agreement with all accessible remotes using fast-foward merges.
# Liam Healy 2015-04-16 08:40:43EDT gitagree.bash
# Time-stamp: <2020-01-10 09:54:36EST gitagree.bash>

# Show any uncommitted changes in the working directory.
# Check the local branches against all available remotes.
# Default option, show differences and proposed fast-forward merge action to bring into agreement.
# With option -c, bring into agreement if it can be done with a fast-forward merge.

# All remotes to be checked must be listed in $USEREMOTES.

# Note: it may be necessary to run in change mode more than once to
# get all remotes and the local repository in agreement.

# Configuration variables
let show=1 # 0=quiet, 1=print to standard output, 2=more details on standard output
let disagree=1 # 0=show both HEAD SHAs; 1=show HEAD SHAs only if different
let action=0 # 1=take action if possible/needed when HEADs different

while getopts ":achqv" opt; do
  case $opt in
    q) # quiet
      let show=0
      ;;
    a) # show all branches (otherwise, just the ones where the local and remote differ)
	  let disagree=0 # 0=show both HEAD SHAs; 1=show HEAD SHAs only if different
	  ;;
    h) # help (unimplemented)
      echo "${0##*/} -achqv" >&2;
      echo "-a show all branches (otherwise, just the branches where the local and remote differ)"
      echo "-c make changes if possible/needed when HEADs different" 
      echo "-q no output"
      echo "-v show SHA1 for both heads"
      exit 1
      ;;
    c) # make changes
	  let action=1 # 1=take action if possible/needed when HEADs different
      ;;
    v) # verbose
      let show=2
      let disagree=0 # 0=show both HEAD SHAs; 1=show HEAD SHAs only if different
      ;;
    \?)
      echo "Invalid option: -$OPTARG; try -h for help" >&2;
      exit 1
      ;;
  esac
done

#if [ ! -z ${USEREMOTES+x} ]; then    # USEREMOTES is not set 
#    echo "USEREMOTES is not set"
#    exit 1
#fi

let code=0

# Check working directory for any uncommitted changes
if [ "$show" -ge 1 ]; then
    git status -s
else
    git status -s &> /dev/null
fi

# Iterate over all the remotes
for remote in $(git remote)
do
    sr=0
    # Skip remotes not on the USEREMOTES list
    for r in $USEREMOTES; do
	if [[ "$r" = "$remote" ]] ; then
	    #if [ "$show" -ge 1 ]
	    #then
		#echo "Use remote $remote on the USEREMOTES list"
	    #fi
	    sr=1 
	    break
	fi
    done
    if [[ $sr -eq 0 ]]; then # skip this remote because it's not on the USEREMOTES list
	continue
    fi

    # Fetch the remote
    GIT_SSH=sshbatch git fetch -q $remote &> /dev/null

    # Find the heads of all local branches, put in localbranch and localsha
    # This is inside the remote loop because the local might get updated through a merge from a previous remote
    declare localhead=($(git ls-remote -h .))
    let i=0
    while [ -n "${localhead[i]}" ]; do
	if [ $((i%2)) -eq 0 ]; then
	    localsha[i/2]=${localhead[i]};
	else
	    localbranch[(i-1)/2]=${localhead[i]};
	fi
	let i=i+1
    done
    let numlocbranch=(i+1)/2

    # For this remote, find the branches and heads and put in remotebranch and remotesha
    declare remotehead=($(git ls-remote -h "$remote" 2> /dev/null))
    let i=0
    while [ -n "${remotehead[i]}" ]; do
	if [ $((i%2)) -eq 0 ]; then
	    remotesha[i/2]=${remotehead[i]};
	else
	    remotebranch[(i-1)/2]=${remotehead[i]};
	fi
	let i=i+1
    done
    let numrembranch=(i+1)/2

    # Iterate over the local and remote branches, and when there is a name match found, do what is requested.
    let i=0
    while  (( i < numlocbranch )) ; do
	let j=0
	while (( j < numrembranch )) ; do
	    if [ "${remotebranch[j]}" == "${localbranch[i]}" ]
	    then
		if [ "${remotesha[j]}" != "${localsha[i]}" ] || [ "$disagree" -eq 0 ]
		then
		    if [ "$show" -ge 2 ]
		    then
		     	echo -e ${localsha[i]} local/${localbranch[i]#refs/heads/} \\n${remotesha[j]} $remote/${remotebranch[j]#refs/heads/}
		    fi
		    if [ ${localsha[i]} == ${remotesha[j]} ]; then
			if [ "$show" -ge 1 ]
			then
			    echo "No action; local/${localbranch[i]#refs/heads/} and $remote/${remotebranch[j]#refs/heads/} agree"
			fi
		    elif $(git merge-base --is-ancestor ${localsha[i]} ${remotesha[j]}) &> /dev/null
		    then
			# local is older than remote: pull
			if [ $(git rev-parse --abbrev-ref HEAD) == ${localbranch[i]#refs/heads/} ]; then
			    # current branch 
			    cmd="git merge --ff-only $remote/${remotebranch[j]#refs/heads/}";
			else
			    cmd="git fetch $remote ${localbranch[i]#refs/heads/}:${localbranch[i]#refs/heads/}"
			fi
			if [ "$action" == 1 ]; then
			    $(GIT_SSH=sshbatch $cmd &> /dev/null)
			    status=$?
			    let code=code+status
			    if [ "$show" -ge 1 ]; then
				if [ $status -eq 0 ]         # Test exit status of last command
				then
				    echo Action succeeded: $cmd
				else 
				    echo Action failed: $cmd
				fi
			    fi
			else
			    if [ "$show" -ge 1 ]; then
				echo Action proposed: $cmd
			    fi
			fi
		    elif $(git merge-base --is-ancestor ${remotesha[j]} ${localsha[i]}) &> /dev/null
 		    then
			# remote is older than local: push
			cmd="git push $remote ${localbranch[i]#refs/heads/}:${remotebranch[j]#refs/heads/}"
			if [ "$action" == 1 ]; then
			    $(GIT_SSH=sshbatch $cmd &> /dev/null)
			    status=$?
			    let code=code+status
			    if [ "$show" -ge 1 ]; then
				if [ $status -eq 0 ]         # Test exit status of last command
				then
				    echo Action succeeded: $cmd
				else 
				    echo Action failed: $cmd
				fi
			    fi
			else
			    if [ "$show" -ge 1 ]
			    then
				echo Action proposed: $cmd
			    fi
			fi
		    else
			if [ "$show" -ge 1 ]
			then
			    echo Manual action needed to align local branch ${localbranch[i]#refs/heads/} with $remote/${remotebranch[j]#refs/heads/}
			    let code=code+2
			fi
		    fi
		fi
	    fi
	    let j=j+1
	done
	let i=i+1
    done
done

exit $code
